import {Formik} from 'formik'
import React, {useCallback, useEffect} from 'react'
import CanvasGrid, {RenderCell} from './canvas'
import GameOfLife, {IGameConfig} from './gol'
import {Cell} from './gol/cells'

class GOLCanvas extends GameOfLife {
  private offsetX: number
  private offsetY: number

  constructor(conf: IGameConfig) {
    super(conf)

    /**
     * We take a ceil of our width/scale because this will give us a round int of how many cells we'll have on X and Y.
     * That will have some overflow. The reason we do this is so there is no negative or 'dead' space from the edge of
     * the cells being rendered, and the edge of the canvas. To keep it centered we determine what the amount of overflow
     * is in pixels, and then we half that so that we can transpose our rendering by that amount, it will always be
     *
     * 0 >= n > scale, where n is the amount of overflow in pixels
     */

    this.offsetX =
      ((conf.width / conf.scale - Math.ceil(conf.width / conf.scale)) *
        conf.scale) /
      2
    this.offsetY =
      ((conf.height / conf.scale - Math.ceil(conf.height / conf.scale)) *
        conf.scale) /
      2
  }

  generateRenderCells = (): RenderCell[] => {
    const renderCells: RenderCell[] = []
    const worldCells = this.cells

    const from = new Date().getMilliseconds()
    const cellsToDraw = this.cells.cells
      .map(v => v.filter(cell => cell.alive))
      .flat()
    const to = new Date().getMilliseconds()

    cellsToDraw.forEach((cell: Cell) => {
      renderCells.push({
        x: Math.round(cell.x * worldCells.scale + this.offsetX),
        y: Math.round(cell.y * worldCells.scale + this.offsetY),
        // fill: '#000',
        height: worldCells.scale,
        width: worldCells.scale
      })
    })

    return renderCells
  }
}

const GOLConfig: IGameConfig = {
  height: 0,
  width: 0,
  scale: 8
}

let GOL: GOLCanvas

let request: number
let runTime: number
let animationDelta: number
let frameDelta: number

const animate = (time: number): void => {
  request = requestAnimationFrame(animate)
  animationDelta = time - runTime
  frameDelta = animationDelta + (frameDelta ? frameDelta : 0)

  GOL.requestTick()

  if (GOL && frameDelta >= 100) {
    /**
     * Dump the GOL cells and call all of the listeners waiting for the tick event
     * this will allow canvas to draw the frame.
     */
    GOL.applyListeners('OnTick', GOL.dump())

    frameDelta = 0
  }

  runTime = time
}

const Conway = (props: React.HTMLAttributes<HTMLDivElement>) => {
  const {children, style, ...otherProps} = props

  const ref = React.useRef<HTMLDivElement>(null)

  const [canvHeight, setCanvHeight] = React.useState(0)
  const [canvWidth, setCanvWidth] = React.useState(0)
  const [canvBuf, setCanvBuf] = React.useState<RenderCell[]>()

  // const configAndStartGOL = useCallback((config: IGameConfig) => {})

  useEffect(() => {
    GOLConfig.height = ref.current ? ref.current.clientHeight : 0
    GOLConfig.width = ref.current ? ref.current.clientWidth : 0

    setCanvHeight(GOLConfig.height)
    setCanvWidth(GOLConfig.width)

    GOL = new GOLCanvas(GOLConfig)

    GOL.listen('OnTick', state => setCanvBuf(GOL.generateRenderCells()))
    setCanvBuf(GOL.generateRenderCells())

    requestAnimationFrame(animate)
    return cancelAnimationFrame(request)
  }, [])

  useEffect(() => {
    const height = ref.current?.clientHeight
    const width = ref.current?.clientWidth
    if (height && width) {
      setCanvHeight(height)
      setCanvWidth(width)

      GOL.resize(height, width)
    }
  }, [ref.current])

  return (
    <div ref={ref} style={{position: 'relative', ...style}} {...otherProps}>
      <div style={{zIndex: 2, position: 'inherit'}}>{children}</div>
      <CanvasGrid
        grid={true}
        cells={canvBuf}
        style={{position: 'absolute', top: 0, left: 0}}
        height={canvHeight}
        width={canvWidth}
      />
    </div>
  )
}

export default Conway
