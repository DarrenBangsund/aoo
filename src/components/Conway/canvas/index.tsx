import React, {useEffect} from 'react'
import {Pixel} from '../gol/pixel'

export interface RenderCell extends Pixel {
  height: number
  width: number
  fill?: CanvasFillStrokeStyles['fillStyle']
}

interface ICanvasGridProps
  extends React.CanvasHTMLAttributes<HTMLCanvasElement> {
  cells?: RenderCell[]
  grid?: boolean
}

const CanvasGrid = (props: ICanvasGridProps) => {
  const {cells, grid, ...otherProps} = props
  const canvasRef = React.useRef<HTMLCanvasElement>(null)

  // React.useEffect(() => {
  //   const canvas = canvasRef.current
  //   const ctx = canvas?.getContext('2d')
  //   if (ctx && grid && cells) {
  //     for (const i in cells) {
  //       const el = cells[i]

  //       const x = el.x
  //       const y = el.y

  //       ctx.strokeStyle = '#000000'
  //       ctx.beginPath()
  //       ctx.moveTo(x, y)
  //       ctx.lineTo(x + el.height, y)
  //       ctx.stroke()

  //       ctx.beginPath()
  //       ctx.moveTo(x, y + el.height)
  //       ctx.lineTo(x + el.height, y + el.height)
  //       ctx.stroke()

  //       ctx.beginPath()
  //       ctx.moveTo(x, y)
  //       ctx.lineTo(x, y + el.width)
  //       ctx.stroke()

  //       ctx.beginPath()
  //       ctx.moveTo(x + el.width, y)
  //       ctx.lineTo(x + el.width, y + el.width)
  //       ctx.stroke()
  //     }
  //   }
  // }, [grid, cells])

  useEffect(() => {
    const canvas = canvasRef.current
    const ctx = canvas?.getContext('2d')

    if (canvas && ctx && cells) {
      ctx.clearRect(0, 0, canvas?.width, canvas?.height)
      ctx.fillStyle = '#fff'
      ctx.fillRect(0, 0, canvas.width, canvas.height)

      for (const i in cells) {
        const el = cells[i]

        const x = el.x
        const y = el.y

        ctx.beginPath()
        ctx.fillStyle = '#000'
        ctx.rect(x, y, el.width, el.height)
        ctx.fill()
        ctx.closePath()
      }
    }
  }, [cells])

  return <canvas ref={canvasRef} {...otherProps} />
}

export default CanvasGrid
