import React from 'react'

import Container from '@material-ui/core/Container'
// import { createGlobalStyle } from "styled-components";
import {withStyles} from '@material-ui/styles'
import {Box, Typography} from '@material-ui/core'
// import {withStyles} from '@material-ui/core/styles'

import Conway from '../components/Conway'

// const GlobalStyle = createGlobalStyle`
//   body {
//     background: ${(props) => props.theme.background};
//   }
// `;

const resumeStyles = {}

const Resume = (): JSX.Element => (
  <Conway>
    <Container
      maxWidth='md'
      style={{
        // outline: '1px solid black'
        background: 'white',
        minHeight: '100vh',
        boxShadow: 'inset 0px 0px 2px 1px rgba(0,0,0,0.5)',
        overflow: 'hidden'
      }}
    >
      <div>My conway test is going well</div>
    </Container>
  </Conway>
)

export default withStyles(resumeStyles)(Resume)
