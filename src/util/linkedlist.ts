const CursorEmptyError = 'Could not construct cursor from empty LinkedList'

interface LinkedListNode<T> {
  item?: T
  prev?: LinkedListNode<T>
  next?: LinkedListNode<T>
}

export class Cursor<T> {
  private list: LinkedList<T>
  private current: LinkedListNode<T> | undefined
  private idx: number

  constructor(list: LinkedList<T>) {
    this.idx = -1
    this.current = list.head
    this.list = list
  }

  get = (): T | undefined => {
    return this.current?.item
  }

  next = (): T | undefined => {
    if (this.idx === -1) {
      this.current = this.list.head
      this.idx = this.idx + 1
      return this.current?.item
    }

    if (this.idx === this.list.length) {
      this.reset()
      return undefined
    }

    this.idx = this.idx + 1
    this.current = this.current?.next

    return this.current?.item
  }

  prev = (): T | undefined => {
    if (this.idx === this.list.length || this.idx === -1) {
      this.current = this.list.tail
      this.idx = this.idx - 1
      return this.current?.item
    }

    if (this.idx === 0) {
      this.reset()
      return undefined
    }

    this.idx = this.idx - 1
    this.current = this.current?.prev

    return this.current?.item
  }

  reset = (): void => {
    this.idx = -1
  }
}

class LinkedList<T> {
  public head: LinkedListNode<T> | undefined
  public tail: LinkedListNode<T> | undefined
  public length: number

  constructor() {
    this.head = undefined
    this.tail = undefined
    this.length = 0
  }

  push = (item: T): void => {
    if (!this.head && !this.tail) {
      const firstItem: LinkedListNode<T> = {item}

      this.head = firstItem
      this.tail = firstItem

      return
    }

    const newItem: LinkedListNode<T> = {item}

    if (this.head && this.head === this.tail) {
      this.head.next = newItem
    }

    const buf = this.tail

    if (buf) {
      newItem.prev = buf
      this.tail = newItem
      buf.next = this.tail
    }

    this.length += 1

    return
  }

  cursor = (): Cursor<T> => {
    if (this.head && this.tail) {
      return new Cursor<T>(this)
    }

    throw CursorEmptyError
  }
}

export default LinkedList
