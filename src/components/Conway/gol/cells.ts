import {Pixel} from './pixel'

export interface Cell extends Pixel {
  alive: boolean
}

export interface ICellGrid {
  cells: Cell[][]
  height: number
  width: number
  scale: number
  forEach: (fn: (value: Cell) => void) => void
  randomize: (chance: number) => void
  neighbors: (x: number, y: number) => Cell[]
  get: (x: number, y: number) => Cell | undefined
  clone: () => ICellGrid
  resizeCells: (height: number, width: number) => void
}

export class CellGrid {
  cells: Cell[][]
  height: number
  width: number
  scale: number

  constructor(height: number, width: number, scale: number) {
    this.height = height
    this.width = width
    this.scale = scale
    this.cells = []
    this.cells = this.generateCells()
  }

  generateCells = (): Cell[][] => {
    let cells = this.cells && this.cells.length > 0 ? this.cells : []
    const cellsX = Math.ceil(this.width / this.scale)
    const cellsY = Math.ceil(this.height / this.scale)
    const cellsXDelta = cellsX - cells.length

    //size X
    if (cellsXDelta > 0) {
      for (let x = 0; x < cellsXDelta; x++) {
        const newX = Array<Cell>()
        cells.push(newX)
      }
    } else if (cellsXDelta < 0) {
      cells = cells.slice(0, cellsXDelta)
    }

    for (let x = 0; x < cells.length; x++) {
      const X = cells[x]
      const cellsYDelta = cellsY - X.length

      if (cellsYDelta > 0) {
        for (let y = 0; y < cellsYDelta; y++) {
          const newCell: Cell = {x, y, alive: false}
          X.push(newCell)
        }
      } else if (cellsYDelta < 0) {
        cells[x] = X.slice(cellsYDelta)
      }
    }

    return cells
  }

  resizeCells = (height: number, width: number): void => {
    this.width = width
    this.height = height
    this.cells = this.generateCells()
  }

  forEach = (fn: (value: Cell) => void) => {
    for (let x = 0; x < this.cells.length; x++) {
      for (let y = 0; y < this.cells[x].length; y++) {
        fn(this.cells[x][y])
      }
    }
  }

  randomize = (chance: number) => {
    this.forEach(cell => {
      if (Math.random() <= chance) {
        this.cells[cell.x][cell.y].alive = true
      } else {
        this.cells[cell.x][cell.y].alive = false
      }
    })
  }

  get = (x: number, y: number): Cell | undefined => {
    if (x >= 0 && x < this.cells.length) {
      if (y >= 0 && y < this.cells[x].length) {
        return this.cells[x][y]
      }
    }

    return undefined
  }

  neighbors = (x: number, y: number): Cell[] => {
    const out: Cell[] = []
    const n: Pixel[] = [
      {x: x - 1, y: y + 1}, //top left corner
      {x: x, y: y + 1}, //top
      {x: x + 1, y: y + 1}, //top right corner
      {x: x - 1, y: y}, //left
      // {x: x,y},
      {x: x + 1, y: y}, //right
      {x: x - 1, y: y - 1}, //bottom left corner
      {x: x, y: y - 1}, //bottom
      {x: x + 1, y: y - 1} //bottom right corner
    ]

    n.forEach((p: Pixel) => {
      const v = this.get(p.x, p.y)
      if (v) {
        out.push(v)
      }
    })

    return out
  }

  apply = (c: Cell[][]): void => {
    this.cells = c
  }

  clone = (): ICellGrid => {
    const c = JSON.parse(JSON.stringify(this.cells))
    const g = new CellGrid(this.height, this.width, this.scale)
    g.apply(c)

    return g
  }

  resize = (height: number, width: number): void => {
    // const oldHeight = this.height
    // const oldWidth = this.width
    // if
  }
}
