import React from 'react'
import {Router, Route} from 'react-router-dom'
import {createBrowserHistory} from 'history'
import Resume from './containers/resume'
import Layout from './components/Layout'

const history = createBrowserHistory()

function App(): JSX.Element {
  return (
    <Layout>
      <Router history={history}>
        {/* <Route path='/resume'>
        </Route> */}
        <Route>
          <Resume />
        </Route>
      </Router>
    </Layout>
  )
}

export default App
