import {Cell, CellGrid, ICellGrid} from './cells'

enum EListenerTypes {
  'OnTick'
}

type ListenerTypes = keyof typeof EListenerTypes
type Listenerfn = (state: ICellGrid) => void

export interface IListeners {
  [key: string]: Listenerfn[]
}

export interface IGameConfig {
  height: number
  width: number
  scale: number
}

export default class GameOfLife {
  protected config: IGameConfig
  protected cells: ICellGrid
  protected listeners: IListeners
  protected canTick: boolean

  constructor(conf: IGameConfig) {
    const cellGrid = new CellGrid(conf.height, conf.width, conf.scale)
    cellGrid.randomize(0.25)

    this.config = conf
    this.canTick = false
    this.cells = cellGrid
    this.listeners = {}

    for (const t in EListenerTypes) {
      this.listeners[t] = []
    }
  }

  print = (): void => {
    let s = ''
    for (let x = 0; x < this.cells.cells.length; x++) {
      for (let y = 0; y < this.cells.cells[x].length; y++) {
        const c = this.cells.cells[x][y]
        let str = c.alive ? 'x' : 'o'
        str += this.cells.get(c.x, c.y + 1) ? ' | ' : ''
        s += str
      }

      s += '\n'
    }

    console.log(s)
  }

  requestTick = (): void => {
    if (this.canTick) {
      const newGrid = new CellGrid(
        this.cells.height,
        this.cells.width,
        this.cells.scale
      )

      this.cells.forEach((cell: Cell) => {
        const neighbors = this.cells.neighbors(cell.x, cell.y)
        const aliveNeighbors = neighbors.filter((v: Cell) => v.alive)

        const newCell = newGrid.get(cell.x, cell.y)

        if (newCell) {
          if (cell.alive) {
            if (aliveNeighbors.length < 2) {
              newCell.alive = false
            } else if (aliveNeighbors.length > 1 && aliveNeighbors.length < 4) {
              newCell.alive = true
            } else {
              newCell.alive = false
            }
          } else {
            if (aliveNeighbors.length === 3) {
              newCell.alive = true
            }
          }
        }
      })

      //TODO: implement immutable stack
      this.cells = newGrid
      this.canTick = false
    }
  }

  resize = (height: number, width: number): void =>
    this.cells.resizeCells(height, width)

  listen = (listener: ListenerTypes, listenerfn: Listenerfn) =>
    this.listeners[listener].push(listenerfn)

  applyListeners = (listener: ListenerTypes, ...args: any) =>
    this.listeners[listener].forEach(fn => fn(args))

  dump = (): ICellGrid => {
    this.canTick = true
    return this.cells
  }
}
