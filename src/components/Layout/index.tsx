import React, {ReactNode} from 'react'
import {Grid} from '@material-ui/core'

interface LayoutProps {
  children: ReactNode
}

const Layout = ({children}: LayoutProps): JSX.Element => (
  <Grid container direction='column'>
    {/* <Grid item>asdf</Grid> */}
    <Grid item>{children}</Grid>
  </Grid>
)

export default Layout
